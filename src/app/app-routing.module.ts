import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { WorkComponent } from './components/work/work.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const routes: Routes = [{
  path: 'work',
  component: WorkComponent
}, {
  path: 'about',
  component: AboutComponent
}, {
  path: 'contact',
  component: ContactComponent
}, {
  path: '',
  component: DashboardComponent
}
// {
//   path: '',
//   redirectTo: '/work',
//   pathMatch: 'full'
// }
];


@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})


export class AppRoutingModule { }
