import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavComponent } from './components/nav/nav.component';
import { WorkComponent } from './components/work/work.component';
import { LabelComponent } from './components/label/label.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { WorkDetailsComponent } from './components/work-details/work-details.component';
import { WorkDetailsGraphicsComponent } from './components/work-details-graphics/work-details-graphics.component';
import { LightBoxComponent } from './components/light-box/light-box.component';
import { WorkDetailsGamesComponent } from './components/work-details-games/work-details-games.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    WorkComponent,
    LabelComponent,
    AboutComponent,
    ContactComponent,
    WorkDetailsComponent,
    WorkDetailsGraphicsComponent,
    LightBoxComponent,
    WorkDetailsGamesComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
