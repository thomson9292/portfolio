import { Component, OnInit } from '@angular/core';
import { LabelService } from '../../services/label.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private labelservice: LabelService) { }

  showElementsOnScroll() {
    $(document).scroll(function () {
      const window_height = $(window).height();
      const window_top_position = $(window).scrollTop();
      const window_bottom_position = (window_top_position + window_height);

      if (window_bottom_position > ($('.box-personal-1').position().top - 100)) {
        $('#label-personal').animate({
          top: '0'
        });
      }

      if (window_bottom_position > ($('.box-personal-1').position().top + 200)) {
        $('.box-personal-1').children('.under-heading1').children('.under-heading-inner1').animate({
          top: '0'
        }, 1000);
        $('.box-personal-1').children('#zpsb-img').css('transform', 'scale(1)');
      }

      if (window_bottom_position > ($('.box-personal-2').position().top + 200)) {
        $('.box-personal-2').children('.under-heading2').children('.under-heading-inner2').animate({
          top: '0'
        }, 1000);
        $('.box-personal-2').children('.img-pum1').animate({
          left: '0'
        }, 1000);
        $('.box-personal-2').children('.img-pum2').animate({
          right: '0'
        }, 1000);
      }

      if (window_bottom_position > ($('.box-personal-3').position().top + 200)) {
        $('.box-personal-3').children('.under-heading3').children('.under-heading-inner3').animate({
          top: '0'
        }, 1000);
      }

      if (window_bottom_position > ($('#label-web').position().top + $('#label-web').height())) {
        $('#label-web').animate({
          left: '0'
        });
      }

      if (window_bottom_position > ($('#label-game').position().top + $('#label-game').height())) {
        $('#label-game').animate({
          left: '0'
        });
      }

      if (window_bottom_position > ($('.box1').position().top + $('.box1').height() / 2)) {
        $('.box1').each(function (i) {
          const delay = (i) * 500;
          setTimeout(function (div) {
            div.animate({
              right: '0'
            });
          }, delay, $(this));
        });
      }

      if (window_bottom_position > ($('.box2').position().top + $('.box2').height() / 2)) {
        $($('.box2').get().reverse()).each(function (i) {
          const delay = (i) * 500;
          setTimeout(function (div) {
            div.animate({
              left: '0'
            });
          }, delay, $(this));
        });
      }


    });
  }

  ngOnInit() {
    this.labelservice.toggleLabel('ABOUT');
    this.showElementsOnScroll();
    $(window).scrollTop(0);
  }

}
