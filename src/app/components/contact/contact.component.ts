import { Component, OnInit } from '@angular/core';
import { LabelService } from '../../services/label.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor(private labelservice: LabelService) { }

  ngOnInit() {
    this.labelservice.toggleLabel('CONTACT');
    $(window).scrollTop(0);
  }

}
