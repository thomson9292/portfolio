import { Component, OnInit } from '@angular/core';
import { LabelService } from '../../services/label.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private labelservice: LabelService) { }

  ngOnInit() {
    this.labelservice.toggleLabel('hide');

    $(document).ready(function () {
      $('.container-inner').animate({
        top: '-400px'
      }, 2000, function () {
        $(['.logo-box1', '.logo-box2', '.logo-box3', '.logo-box4']).each(function (i) {
          const delay = (i) * 500;
          setTimeout(function (div) {
            div.animate({
              opacity: '1'
            }, 2000);
          }, delay, $(this));

        });
      });
    });
  }

}
