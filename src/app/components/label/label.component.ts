import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { LabelService } from '../../services/label.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {

  constructor(private labelservice: LabelService) { }

  hideLabel() {

  }

  labelChange() {
    this.labelservice.labelOnChange.subscribe(value => {
      if (value[0] === 'hide') {
        $('.label-icon').animate({ top: '-400px' }, 300);
        $('.title').animate({ top: '400px' }, 300, function () {
          $('.elements-' + value[1]).css('display', 'none');
          $('.label-inner').animate({ width: '0' }, 100, function () {
            $(this).removeClass('label-' + value[1]);
          });
        });
        this.labelservice.labelToHide = undefined;
        this.labelservice.labelToShow = undefined;
      }
      if (value[1] !== undefined && value[1] !== value[0]) {
        $('.label-icon').animate({ top: '-400px' }, 300);
        $('.title').animate({ top: '400px' }, 300, function () {
          $('.elements-' + value[1]).css('display', 'none');
          $('.label-inner').animate({ width: '0' }, 100, function () {
            $(this).removeClass('label-' + value[1]);
            $(this).addClass('label-' + value[0]);
            $('.elements-' + value[0]).css('display', 'block');
            $('.label-inner').animate({ width: '50%' }, 300, function () {
              $('.label-icon').animate({ top: '0' }, 300);
              $('.title').animate({ top: '0' }, 300);
            });
          });
        });
        this.labelservice.labelToHide = value[0];
      } else if (value[1] === undefined) {
        console.log(value[0]);
        $('.elements-' + value[0]).css('display', 'block');
        $('.label-inner').addClass('label-' + value[0]);
        $('.label-inner').animate({ width: '50%' }, 300, function () {
          $('.label-icon').animate({ top: '0' }, 300);
          $('.title').animate({ top: '0' }, 300);
        });
        this.labelservice.labelToHide = value[0];
      }
    });
  }

  ngOnInit() {
    this.labelChange();
  }

}
