import { Component, OnInit } from '@angular/core';
import GRAPHICS from '../../constans/graphics';
import WEB from '../../constans/web';
import GAMES from '../../constans/games';
import { LightBoxService } from '../../services/light-box.service';

@Component({
  selector: 'app-light-box',
  templateUrl: './light-box.component.html',
  styleUrls: ['./light-box.component.scss']
})
export class LightBoxComponent implements OnInit {

  photos = { graphics: [], web: [], games: [] };
  path = '';

  constructor(private lightBoxService: LightBoxService) {
    this.photos.graphics = GRAPHICS;
    this.photos.web = WEB;
    this.photos.games = GAMES;
  }

  photo() {
    this.lightBoxService.photoOnChange.subscribe(value => {
      if (value[0] === 'graphics') {
        this.path = this.photos.graphics[value[1].toString()].photo;
        $('.outer').css('display', 'block');
        // $('body').css('overflow', 'hidden');
      } else if (value[0] === 'web') {
        this.path = this.photos.web[value[1].toString()].photo;
        $('.outer').css('display', 'block');
        // $('body').css('overflow', 'hidden');
      } else if (value[0] === 'game') {
        this.path = this.photos.games[value[1].toString()].photo;
        $('.outer').css('display', 'block');
        // $('body').css('overflow', 'hidden');
      }
    });
  }

  exitGallery() {
    $('.outer').css('display', 'none');
    // $('body').css('overflow', 'auto');
  }

  ngOnInit() {
    this.photo();

    // console.log(this.photos.graphics[1].photo);
  }

}
