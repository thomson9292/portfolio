import { Component, OnInit } from '@angular/core';
import { LabelService } from '../../services/label.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(private labelservice: LabelService) { }

  // changeLabel(event) {
  //   // this.labelservice.toggleLabel(event);
  // }

  showUpBtn() {
    $(window).on('scroll', function () {
      const window_height = $(window).height();
      const window_top_position = $(window).scrollTop();
      const window_bottom_position = (window_top_position + window_height);
      // if (window_bottom_position === $(document).height()) {
      if (window_top_position > 1) {
        console.log($(document).height());
        $('#go-up').css('height', '50px');
        $('#go-up').children('.icon-up').fadeIn('slow');
      } else {
        $('#go-up').children('.icon-up').fadeOut('slow');
        $('#go-up').css('height', '0px');
      }
    });
  }

  goUp() {
    // const el = $('#box').position().top + $('#box').height();
    $('html,body').animate({ scrollTop: 0 }, 'slow');
    return false;
  }

  ngOnInit() {
    this.showUpBtn();
  }

}
