import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkDetailsGamesComponent } from './work-details-games.component';

describe('WorkDetailsGamesComponent', () => {
  let component: WorkDetailsGamesComponent;
  let fixture: ComponentFixture<WorkDetailsGamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkDetailsGamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkDetailsGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
