import { Component, OnInit } from '@angular/core';
import { LightBoxService } from '../../services/light-box.service';

@Component({
  selector: 'app-work-details-games',
  templateUrl: './work-details-games.component.html',
  styleUrls: ['./work-details-games.component.scss']
})
export class WorkDetailsGamesComponent implements OnInit {

  constructor(private lightBoxService: LightBoxService) { }

  togglePhoto(id, file) {
    this.lightBoxService.togglePhoto(file, id);
  }

  ngOnInit() {
  }

}
