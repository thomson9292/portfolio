import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkDetailsGraphicsComponent } from './work-details-graphics.component';

describe('WorkDetailsGraphicsComponent', () => {
  let component: WorkDetailsGraphicsComponent;
  let fixture: ComponentFixture<WorkDetailsGraphicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkDetailsGraphicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkDetailsGraphicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
