import { Component, OnInit } from '@angular/core';
import { LightBoxService } from '../../services/light-box.service';

@Component({
  selector: 'app-work-details-graphics',
  templateUrl: './work-details-graphics.component.html',
  styleUrls: ['./work-details-graphics.component.scss']
})
export class WorkDetailsGraphicsComponent implements OnInit {

  constructor(private lightBoxService: LightBoxService) {

  }

  togglePhoto(id, file) {
    this.lightBoxService.togglePhoto(file, id);
  }

  ngOnInit() {
  }

}
