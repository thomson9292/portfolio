import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { LightBoxService } from '../../services/light-box.service';

@Component({
  selector: 'app-work-details',
  templateUrl: './work-details.component.html',
  styleUrls: ['./work-details.component.scss']
})
export class WorkDetailsComponent implements OnInit {

  @Input() offer;
  lastOpened: number;

  constructor(private lightBoxService: LightBoxService) { }

  togglePhoto(id, file) {
    this.lightBoxService.togglePhoto(file, id);
  }

  showOffer(offer, lastOpened?) {
    if (this.lastOpened) {
      $('.inner' + lastOpened).animate({
        right: '-1900px'
      }, function () {
        $(this).parent('.offer' + lastOpened).slideUp('fast', function () {
          $('.offer' + offer).show('fast', function showNext() {
            $(this).children('.inner' + offer).show('fast', function showNext2() {
              $(this).animate({
                right: '0'
              }, 1000);
            });
            // $([document.documentElement, document.body]).animate({
            //   scrollTop: $('.offer' + offer).offset().top
            // }, 1000);
          });
        });
      });
      $([document.documentElement, document.body]).animate({
        scrollTop: $('#height-box').offset().top
      }, 1000);
    } else {
      $('.offer' + offer).show('fast', function showNext() {
        $(this).children('.inner' + offer).show('fast', function showNext2() {
          $(this).animate({
            right: '0'
          }, 1000);
        });
        // $([document.documentElement, document.body]).animate({
        //   scrollTop: $('#height-box').offset().top
        // }, 1000);
      });
      $('#height-box').show('fast');
      $([document.documentElement, document.body]).animate({
        scrollTop: $('#height-box').offset().top
      }, 1000);
    }

    this.lastOpened = offer;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    switch (this.offer) {
      case 1: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      case 2: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      case 3: {
        this.showOffer(this.offer, this.lastOpened);
        break;
      }
      default: {
        break;
      }
    }
  }

  changeColor() {
    $('.item').mouseover(function () {
      $(this).siblings('.item').not(this).css('filter', 'brightness(0.3)');
    });
    $('.item').mouseleave(function () {
      $(this).siblings('.item').css('filter', 'brightness(1)');
    });
  }

  ngOnInit() {
    // this.showElementsOnScroll();
    this.changeColor();
  }

}
