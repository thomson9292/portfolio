import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { LabelService } from '../../services/label.service';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.scss']
})
export class WorkComponent implements OnInit {

  offerToDisplay: number;

  constructor(private labelservice: LabelService) { }

  openOffer(event) {
    this.offerToDisplay = event;
  }

  showBox() {
    $('.element').each(function () {
      $(this).mouseover(function () {
        $(this).children('.content').children('.icon-up').animate({ top: '0' }, 300);
        $(this).children('.content').children('.icon-btm').animate({ bottom: '0' }, 300);
      });
      $(this).mouseleave(function () {
        $(this).children('.content').children('.icon-up').animate({ top: '-170px' }, 300);
        $(this).children('.content').children('.icon-btm').animate({ bottom: '-170px' }, 300);
      });
    });
  }

  ngOnInit() {
    this.showBox();
    this.labelservice.toggleLabel('WORK');
    $(window).scrollTop(0);
  }

}
