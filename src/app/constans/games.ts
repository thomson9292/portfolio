export default [
  {
    'id': '1',
    'photo': '../../../assets/img/game/turbo1.jpg',
    'photo-thumb': '../../../assets/img/game/turbo1.jpg',
    'text': '',
  },
  {
    'id': '2',
    'photo': '../../../assets/img/game/code1.png',
    'photo-thumb': '../../../assets/img/game/code1.png',
    'text': '',
  },
  {
    'id': '3',
    'photo': '../../../assets/img/game/turbo-alien.gif',
    'photo-thumb': '../../../assets/img/game/turbo-alien.gif',
    'text': '',
  },
  {
    'id': '4',
    'photo': '../../../assets/img/game/game2.jpg',
    'photo-thumb': '../../../assets/img/game/game2.jpg',
    'text': '',
  },
  {
    'id': '5',
    'photo': '../../../assets/img/game/turbo2.jpg',
    'photo-thumb': '../../../assets/img/game/turbo2.jpg',
    'text': '',
  },
  {
    'id': '6',
    'photo': '../../../assets/img/game/game3.jpg',
    'photo-thumb': '../../../assets/img/game/game3.jpg',
    'text': '',
  },
  {
    'id': '7',
    'photo': '../../../assets/img/game/orc.jpg',
    'photo-thumb': '../../../assets/img/game/orc.jpg',
    'text': '',
  },
]
