export default [
  {
    'id': '1',
    'photo': '../../../assets/img/web1/ProjektMari blog short.jpg',
    'photo-thumb': '../../../assets/img/web1/ProjektMari blog short.jpg',
    'text': '',
  },
  {
    'id': '2',
    'photo': '../../../assets/img/web1/ProjektMari oferta.jpg',
    'photo-thumb': '../../../assets/img/web1/ProjektMari oferta.jpg',
    'text': '',
  },
  {
    'id': '3',
    'photo': '../../../assets/img/web1/ProjektMari mainPage short.jpg',
    'photo-thumb': '../../../assets/img/web1/ProjektMari mainPage short.jpg',
    'text': '',
  },
  {
    'id': '4',
    'photo': '../../../assets/img/web1/ProjektMari mainPage short2.jpg',
    'photo-thumb': '../../../assets/img/web1/ProjektMari mainPage short2.jpg',
    'text': '',
  },
  {
    'id': '5',
    'photo': '../../../assets/img/web1/ProjektMari oMnie short.jpg',
    'photo-thumb': '../../../assets/img/web1/ProjektMari oMnie short.jpg',
    'text': '',
  },
  {
    'id': '6',
    'photo': '../../../assets/img/web2/trenerzy.jpg',
    'photo-thumb': '../../../assets/img/web2/trenerzy.jpg',
    'text': '',
  }
];
