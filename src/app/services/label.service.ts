import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LabelService {

  labelToShow;
  labelToHide;
  labelOnChange: Subject<any[]> = new Subject<any>();


  constructor() {
    this.labelOnChange.subscribe((value) => {
      this.labelToShow = value[0];
      this.labelToHide = value[1];
      console.log(value);
    });
  }


  toggleLabel(text) {
      this.labelToShow = text.toLowerCase();
      this.labelOnChange.next([this.labelToShow, this.labelToHide]);
  }

}
