import { TestBed } from '@angular/core/testing';

import { LightBoxService } from './light-box.service';

describe('LightBoxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LightBoxService = TestBed.get(LightBoxService);
    expect(service).toBeTruthy();
  });
});
