import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LightBoxService {

  photoToShow;
  photoOnChange: Subject<any[]> = new Subject<any>();

  constructor() {
    this.photoOnChange.subscribe((value) => {
      this.photoToShow = value;
    });
  }

  togglePhoto(path, file) {
    console.log(file);
    this.photoOnChange.next([path, file]);
  }

}
